﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bowling.Tests
{
    [TestClass]
    public class GameTests
    {
        private Game game;

        [TestInitialize]
        public void Initialize()
        {
            game = new Game();
        }

        [TestMethod]
        public void can_roll_all_zeros()
        {
            RollMany(20, 0);

            Assert.AreEqual(0, game.GetScore());
        }

        [TestMethod]
        public void can_roll_all_one()
        {
            RollMany(20, 1);

            Assert.AreEqual(20, game.GetScore());
        }

        [TestMethod]
        public void can_spare()
        {
            game.Roll(5);
            game.Roll(5);
            RollMany(18, 0);

            Assert.AreEqual(10, game.GetScore());
        }

        [TestMethod]
        public void can_spare_and_get_bonus()
        {
            game.Roll(5);
            game.Roll(5);
            game.Roll(2);
            RollMany(17, 0);

            Assert.AreEqual(14, game.GetScore());
        }

        [TestMethod]
        public void can_strike_on_first_roll()
        {
            game.Roll(10);
            RollMany(19, 0);
            Assert.AreEqual(10, game.GetScore());
        }

        [TestMethod]
        public void can_strike_on_second_roll()
        {
            game.Roll(0);
            game.Roll(10);
            RollMany(18, 0);
            Assert.AreEqual(10, game.GetScore());
        }

        [TestMethod]
        public void can_strike_and_get_bonus()
        {
            game.Roll(10);
            game.Roll(1);
            game.Roll(2);
            RollMany(17, 0);

            Assert.AreEqual(16, game.GetScore());
        }

        [TestMethod]
        public void can_get_maximum_score()
        {
            RollMany(12, 10);

            Assert.AreEqual(300, game.GetScore());
        }

        [TestMethod]
        public void can_extra_roll_in_last_frame_if_spare()
        {
            RollMany(18, 0);
            game.Roll(5);
            game.Roll(5);
            game.Roll(5);

            Assert.AreEqual(20, game.GetScore());
        }

        [TestMethod]
        public void can_extra_roll_in_last_frame_if_strike()
        {
            RollMany(18, 0);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);

            Assert.AreEqual(30, game.GetScore());
        }

        private void RollMany(int n, int pins)
        {
            for (var i = 1; i <= n; i++)
                game.Roll(pins);
        }
    }
}
