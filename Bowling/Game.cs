﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class Game
    {
        private int rollIndex;
        private const int TOTAL_PINS = 10;
        private const int TOTAL_FRAMES = 10;
        private const int MAX_ROLLS = 21;

        private int[] rolls = new int[MAX_ROLLS];

        public Game()
        {
            for (var i = 0; i < MAX_ROLLS; i++)
                rolls[i] = 0;

            rollIndex = 0;
        }

        public void Roll(int pins)
        {
            rolls[rollIndex++] = pins;
        }

        public int GetScore()
        {
            var totalScore = 0;
            var rollIndex = 0;

            for (var frame = 0; frame < TOTAL_FRAMES; frame++)
            {
                if (IsStrike(rollIndex))
                {
                    totalScore += GetScoreWithBonus(rollIndex);
                    rollIndex++;
                }
                else if (IsSpare(rollIndex))
                {
                    totalScore += GetScoreWithBonus(rollIndex);
                    if (frame == 9)
                        totalScore += rolls[rollIndex + 2];

                    rollIndex += 2;
                }
                else
                {
                    totalScore += GetFrameScore(rollIndex);
                    rollIndex += 2;
                }
            }

            return totalScore;
        }

        private bool IsStrike(int roll)
        {
            return rolls[roll] == TOTAL_PINS;
        }

        private bool IsSpare(int roll)
        {
            return rolls[roll] + rolls[roll + 1] == TOTAL_PINS;
        }

        private int GetFrameScore(int roll)
        {
            return rolls[roll] + rolls[roll + 1];
        }

        private int GetScoreWithBonus(int roll)
        {
            return GetFrameScore(roll) + rolls[roll + 2];
        }
    }
}
